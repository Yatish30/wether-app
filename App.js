/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  useColorScheme,
  Text,
  
} from 'react-native';

import AppNavigator from './source/navigation/navigator';




const App=() =>{


  return (
    <AppNavigator />
  );
};

export default App;
