/**
 * @format
 */

import {AppRegistry} from 'react-native';
import * as React from 'react';
import App from './App';
import {name as appName} from './app.json';

import { Provider } from 'react-redux';

import {store} from './source/redux/store';



const StoreWrapper = () => (
    <Provider store = { store }>
      <App />
    </Provider>
  )


AppRegistry.registerComponent(appName, () => StoreWrapper);
