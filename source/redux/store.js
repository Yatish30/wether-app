import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import app from './reducers/app.reducer';
const rootReducer = combineReducers({
    app,
});
export const store = createStore(rootReducer, applyMiddleware(thunk));