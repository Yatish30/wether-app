
import appActions from '../constants/app.actionTypes';
import  Api from '../../api/home.api';

const fetchDataStart =()=>{
    return{
        type: appActions.WETHER_DATA.START
    }
}


const fetchDataSuccess =(data)=>{
    return{
        type: appActions.WETHER_DATA.SUCCESS,
        data: data,
    }
}

const fetchDataFail =()=>{
    return{
        type: appActions.WETHER_DATA.FAIL
    }
}






export const fetchData =(lat, long) =>{
    return (dispatch)=>{
        dispatch(fetchDataStart());

        Api(lat, long).then(response=>{
            console.log('Response here is in Action',response);
            dispatch(fetchDataSuccess(response.data));

        }).catch(err=>{
            console.log(err);
            dispatch(fetchDataFail('Error here'));

        }
        )

    }
}