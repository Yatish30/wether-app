import appActions from '../constants/app.actionTypes';

const initialState = {
  loading: false,
  success: false,
  error: false,
  data: '',
};

const AppReducer = (state = initialState, action) => {
  switch (action.type) {
    case appActions.WETHER_DATA.START:
      console.log('burrah', action);
      return {
        ...state,
        loading: true,
        success: false,
        error: false,
        data: '',
      };
    case appActions.WETHER_DATA.SUCCESS:
      console.log('burrah', action);
        return {
          ...state,
          loading: false,
          success: true,
          error: false,
          data: action.data,
      }
    case appActions.WETHER_DATA.FAIL:
      console.log('burrah', action);
      return {
        loading: false,
        success: false,
        error: true,
        // data: '',
      };

    default:
      return state;
  }
};
export default AppReducer;
