import { StyleSheet, Platform } from 'react-native';
import { scaling } from '../utils/index';

const styles = StyleSheet.create({

  screen: {
    flex: 1,
    alignItems: 'center',
  },
  upparContainer: {
    height: '38%',
    alignItems:'center'
  },
  temperatureText:{
       fontSize: scaling.normalize(85),
    fontWeight: '600',
    marginTop: scaling.heightScale(40)
  },
  cityText: {
    fontSize: scaling.normalize(25),
    fontWeight: '400',
  },

  wentWrongText: {
    fontSize: scaling.normalize(40),
    fontWeight: '600',
    marginTop: scaling.heightScale(200),
    width:'80%'
  },

});

export default styles;
