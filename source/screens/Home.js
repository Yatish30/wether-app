import React, { useState, useEffect } from 'react';
import {
  View, Text, Alert, TouchableOpacity, Platform, Keyboard,
  PermissionsAndroid
} from 'react-native';
import styles from './HomeScreenStyles';
import {connect} from 'react-redux'
import {Header, DataTab, Button, Loader} from '../components';
import homeApi from '../api/home.api';
import {fetchData} from '../redux/actions/home.action'
import { Persmission, PERMISSION_TYPE } from '../utils/appPermission';
import Geolocation from '@react-native-community/geolocation';



const HomeScreen = (props) => {

  var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  const requestPermission = async (props) => {
    try {
      if (Platform.OS === 'ios') {
        Persmission.requestPermissionStatus(PERMISSION_TYPE.location).then((permission) => {
          if (!permission) {
            Persmission.requestMultiple([PERMISSION_TYPE.location]).then((permissionResult) => {
              if (permissionResult) {
                Geolocation.requestAuthorization();
                Geolocation.getCurrentPosition((info) => {
                  props.getData(info.coords.latitude, info.coords.longitude);
                });
              }
            });
          }
          if (permission) {
            console.log(props);
            Geolocation.requestAuthorization();
                Geolocation.getCurrentPosition((info) => {
                  props.getData(info.coords.latitude, info.coords.longitude);
                });

          }
        }).catch((err)=>{
          console.log('Error Here is ',err)
        });
      }  else {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Mygoldkart Wants To use your device\'s Location',
            message:
            'Please provide location permission to ensure accurate address to find nearest partners and ensure accurate address for product delivery.',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          props.getData();

          Geolocation.getCurrentPosition((info) => {
            props.getData(info.coords.latitude, info.coords.longitude);
          });
          } else {
          console.log('You can not Access Location');
        }
      }
    } catch (err) {
      console.warn(err);
    }
  };




    useEffect(()=>{
      requestPermission(props);
    },[]);

    const renderErrorContainer = ()=>{
      return(
        <View style={styles.screen}>
          <Text style={styles.wentWrongText}>Something went wrong at our end!</Text>
          <Button title='Retry' press={()=>{
            requestPermission(props);
          }}/>
        
        </View>
      );
    }

if(props.loading){
  return <Loader />
}
    
    return(
      props.data && props.data.cod==200?<>
    <View style={styles.screen}>
        <Header name='Whether App' />
        <View style={styles.upparContainer}>
    <Text style={styles.temperatureText}>{props.data.list[1].main.temp_max}</Text>
            <Text style={styles.cityText}>{props.data.city.name}</Text>
        </View>
        {props.data && props.data.list.map((info,index)=>{
          index= index+30;
          if(index%8!==0){
            return;
          }
          var d = new Date(info.dt_txt);
var dayName = days[d.getDay()];

          return <DataTab key={info.dt} day={dayName} temperature={info.main.temp_max} />;

        })}
      </View>
      </>:renderErrorContainer()
      )
};

const mapStateToProps = (state) => ({
  loading: state.app.loading,
  success: state.app.success,
  error: state.app.error,
  data: state.app.data,
  
});
const mapDispatchToProps = (dispatch) => ({

  getData: (lat, long)=> dispatch(fetchData(lat, long))
  

});


export default connect(mapStateToProps,mapDispatchToProps)(HomeScreen);
