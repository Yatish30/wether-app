import { StyleSheet } from 'react-native';
import { scaling } from '../../utils/index';

const styles = StyleSheet.create({
  headerContainer: {
    height: scaling.heightScale(50),
    backgroundColor: 'black',
    width: '100%',
    justifyContent:'center',
    alignItems:'center'
  },

  heading: {
    color: 'white',
    fontSize: scaling.normalize(17),
    marginTop: scaling.heightScale(19)
  },

});

export default styles;
