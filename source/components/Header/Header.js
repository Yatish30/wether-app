/* eslint-disable no-nested-ternary */
import React from 'react';
import {
  View, Text, TouchableOpacity, Image, Platform,
} from 'react-native';
import styles from './HeaderStyles';


/* Header componet from the drawer navigation */
const Header = (props) => (
  <View style={styles.headerContainer}>
    <Text style={styles.heading} >{props.name}</Text>
   
  </View>
);


export default Header;
