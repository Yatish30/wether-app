import { StyleSheet } from 'react-native';
import { scaling  } from '../../utils/index';

const styles = StyleSheet.create({

  lottie: {
    width: scaling.widthScale(200),
    height: scaling.heightScale(200),
    // backgroundColor:'green',
    // color:'green'
  },

  container: {
      flex:1,
    backgroundColor: 'transparent',
    // backgroundColor: 'green',

    alignItems: 'center',
    justifyContent: 'center',
  },
  animationStyle: {
    height: '100%',
    width: '100%',
  },

});

export default styles;
