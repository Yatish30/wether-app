import React from 'react';
import {
  View,
} from 'react-native';
import LottieAnimation from 'lottie-react-native';
import styles from './LoaderStyles';

export default class Loader extends React.PureComponent {
  animation = React.createRef();

  componentDidMount() {
    if (this.animation.current) {
      this.animation.current.play();
    }
  }

  componentDidUpdate(prevProps) {
    const { visible } = this.props;
    if (visible !== prevProps.visible) {
      if (this.animation.current) {
        this.animation.current.play();
      }
    }
  }

  renderLottie = () => (
    <LottieAnimation
      ref={this.animation}
      source={require('./loader.json')}
      loop
      speed={1}
      style={[styles.lottie]}
    />
  );

  render() {
    return (
      <View style={styles.container}>
        {this.renderLottie()}
      </View>
    );
  }
}
