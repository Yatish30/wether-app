import React from 'react';
import {
  Text, TouchableOpacity
} from 'react-native';
import styles from './ButtonStyles';

const Button = (props) => {
  const {
    title, style, titleStyle, press, image,
    gradient,
  } = props;
  return (
    <>
      <TouchableOpacity style={styles.container} onPress={press}>
       
        <Text style={styles.title}>{title}</Text>
    
      </TouchableOpacity>
    </>
  );
};

export default Button;
