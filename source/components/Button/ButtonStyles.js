import { StyleSheet } from 'react-native';
import { scaling } from '../../utils/index';

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    color: 'gray',
    fontSize: scaling.normalize(15),
    lineHeight: scaling.heightScale(18),
  },
  container: {
    flexDirection: 'row',
    height: scaling.heightScale(35),
    width: scaling.widthScale(100),
    justifyContent: 'center',
    alignItems: 'center',
    marginTop:scaling.heightScale(10),
    borderWidth:1,
    borderColor:'black'
  },


});

export default styles;
