import { StyleSheet } from 'react-native';
import { scaling } from '../../utils/index';

const styles = StyleSheet.create({
  conatiner: {
    width:'100%',
    justifyContent:'space-between',
    alignItems:'center',
    flexDirection:'row',
    height: scaling.heightScale(35),
    borderTopWidth: scaling.heightScale(1),
    borderColor: '#8b8680',
  },

text: {

    fontSize: scaling.normalize(20),
    fontWeight: '600',
    color: '#8b8680',
    marginHorizontal: scaling.widthScale(15)
},
});

export default styles;
