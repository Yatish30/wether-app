import Header from './Header/index.js';
import DataTab from './DataTab';
import Loader from './Loader';
import Button from './Button';



export {
    Header,
    DataTab,
    Loader,
    Button,
}
