import React, { useContext, useEffect } from 'react';
import {
  Platform, View, Image, Alert,
} from 'react-native';
import {HomeScreen} from '../screens';
import {home} from './routes';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { navigationRef } from './configureNavigation';


const AppNavigator = (props) => {
    const Stack = createStackNavigator();

  
  return (
    <NavigationContainer ref={navigationRef}>
          <Stack.Navigator
            screenOptions={{
              headerShown: false,
            }}
          >
            <Stack.Screen name={home} component={HomeScreen} />
          </Stack.Navigator>
    </NavigationContainer>
  );
};


export default AppNavigator;
