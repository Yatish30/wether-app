import { create } from 'apisauce'

// define the api
const api = create({
  baseURL: 'https://api.openweathermap.org',
  headers: { Accept: 'application/json' },
})



export default api;