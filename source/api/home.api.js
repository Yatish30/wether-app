import Api from './main';
// Api
//   .get('/data/2.5/forecast?lat=35&lon=139&appid=ea2537f069d0d0dd56a4dc21c8d2eda6')
//   .then(response => console.log('Response',response)).catch(err=>console.log('err', err));

// // customizing headers per-request
// api.post('/users', { name: 'steve' }, { headers: { 'x-gigawatts': '1.21' } })



const GetData=(lat, long)=>Api
.get(`/data/2.5/forecast?lat=${lat}&lon=${long}&appid=ea2537f069d0d0dd56a4dc21c8d2eda6`);


export default GetData;
