import {
    PERMISSIONS, check, RESULTS, request, requestNotifications,
  } from 'react-native-permissions';
  import { Platform } from 'react-native';
  
  const PLATFORM_LOCATION_PERMISSIONS = {
    ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
    android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
  };

  const REQUEST_PERMISSION_TYPE = {
    location: PLATFORM_LOCATION_PERMISSIONS,
  };
  
  const PERMISSION_TYPE = {
    location: 'location',
  };
  
  class AppPermissions {
      checkPermission=async (type):Promise<boolean> => {
        const permissions = REQUEST_PERMISSION_TYPE[type][Platform.OS];
        if (!permissions) {
          return true;
        }
        try {
          const result = await check(permissions);
          if (result == RESULTS.GRANTED) return true;
          return this.requestPermission(permissions);
        } catch (error) {
          return false;
        }
      }
  
      requestPermissionStatus=async (type):Promise<boolean> => {
        const permissions = REQUEST_PERMISSION_TYPE[type][Platform.OS];
        if (!permissions) {
          return true;
        }
        try {
          const result = await check(permissions);
          if (result == RESULTS.GRANTED) return true;
          return false;
          // return this.requestPermission(permissions);
        } catch (error) {
          return false;
        }
      }
  
      requestPermission= async (permissions): Promise<boolean> => {
        try {
          const result = await request(permissions, { message: 'Hello' });
          return result == RESULTS.GRANTED;
        } catch (error) {
          return false;
        }
      }
  
      requestMultiple=async (types): Promise<boolean> => {
        const results = [];
  
        for (const type of types) {
          const permission = REQUEST_PERMISSION_TYPE[type][Platform.OS];
          if (permission) {
            const result = await this.requestPermission(permission);
            results.push(result);
          }
        }
  
        for (const result of results) {
          if (!result) {
            return false;
          }
          return true;
        }
      }
  
      requestNotifyPermission=async (): Promise<boolean> => {
        if (Platform.OS == 'android') {
          return;
        }
        const { status, setting } = await requestNotifications(['alert', 'sound', 'badge']);
        console.log(status, setting);
        return status = RESULTS.GRANTED;
      }
  }
  
  const Persmission = new AppPermissions();
  
  export { Persmission, PERMISSION_TYPE };
  